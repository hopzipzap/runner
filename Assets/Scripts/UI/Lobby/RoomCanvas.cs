﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomCanvas : MonoBehaviour
{
    [SerializeField] private PlayerListingMenu _playerListingMenu;
    [SerializeField] private LeaveRoomMenu _leaveRoomMenu;
    private SwitchCanvas _canvas;

    public void FirstInitialize(SwitchCanvas canvas)
    {
        _canvas = canvas;
        _playerListingMenu.FirstInitialize(canvas);
        _leaveRoomMenu.FirstInitialize(canvas);
    }

    public void Show()
    {
        gameObject.SetActive(true);
    }

    public void Hide()
    {
        gameObject.SetActive(false);
    }
}