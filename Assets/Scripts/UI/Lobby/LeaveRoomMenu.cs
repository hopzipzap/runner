﻿using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;

public class LeaveRoomMenu : MonoBehaviour
{
    private SwitchCanvas _canvas;

    public void FirstInitialize(SwitchCanvas canvas)
    {
        _canvas = canvas;
    }

    public void OnClick_LeaveRoom()
    {
        PhotonNetwork.LeaveRoom(true);
        _canvas.RoomCanvas.Hide();
    }
}