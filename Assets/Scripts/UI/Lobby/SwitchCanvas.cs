﻿using System;
using System.Collections;
using System.Collections.Generic;
using Photon.Realtime;
using UnityEngine;

public class SwitchCanvas : MonoBehaviour
{
    [SerializeField] private LobbyCanvas _lobbyCanvas;
    public LobbyCanvas LobbyCanvas => _lobbyCanvas;
    [SerializeField] private RoomCanvas _roomCanvas;
    public RoomCanvas RoomCanvas => _roomCanvas;

    private void Awake()
    {
        FirstInitialize();
    }

    private void FirstInitialize()
    {
        LobbyCanvas.FirstInitialize(this);
        RoomCanvas.FirstInitialize(this);
    }
}