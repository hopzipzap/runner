﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;

public class RoomListingMenu : MonoBehaviourPunCallbacks
{
    [SerializeField] private Transform _content;
    [SerializeField] private RoomListing _roomListing;

    private List<RoomListing> _listings = new List<RoomListing>();
    private SwitchCanvas _canvas;

    public void FirstInitialize(SwitchCanvas canvas)
    {
        _canvas = canvas;
    }

    public override void OnJoinedRoom()
    {
        _canvas.RoomCanvas.Show();
        _content.DestroyChildren();
        _listings.Clear();
    }

    public override void OnRoomListUpdate(List<RoomInfo> roomList)
    {
        foreach (RoomInfo info in roomList)
        {
            if (info.RemovedFromList)
            {
                int i = _listings.FindIndex(x => x.RoomInfo.Name == info.Name);
                if (i != -1)
                {
                    Destroy(_listings[i].gameObject);
                    _listings.RemoveAt(i);
                }
            }
            else
            {
                int i = _listings.FindIndex(x => x.RoomInfo.Name == info.Name);
                if (i == -1)
                {
                    RoomListing listing = Instantiate(_roomListing, _content);
                    if (listing != null)
                    {
                        listing.SetRoomInfo(info);
                        _listings.Add(listing);
                    }
                }
            }
        }
    }
}