﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LobbyCanvas : MonoBehaviour
{
    [SerializeField] private CreateRoomMenu _createRoomMenu;

    [SerializeField] private RoomListingMenu _roomListingMenu;
    private SwitchCanvas _canvas;

    public void FirstInitialize(SwitchCanvas canvas)
    {
        _canvas = canvas;
        _createRoomMenu.FirstInitialize(canvas);
        _roomListingMenu.FirstInitialize(canvas);
    }
}