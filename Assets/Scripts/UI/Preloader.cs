﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Photon.Pun;
using UnityEngine.Rendering;

public class Preloader : MonoBehaviour
{
    private CanvasGroup _fadeGroup;
    private float loadTime;
    private float minLogoTime = 3.0f;

    private void Awake()
    {
        Screen.orientation = ScreenOrientation.Landscape;
        Screen.autorotateToPortrait = false;
        Screen.autorotateToPortraitUpsideDown = false;
    }

    private void Start()
    {
        _fadeGroup = FindObjectOfType<CanvasGroup>();
        
        _fadeGroup.alpha = 1;
        if (Time.time < minLogoTime)
            loadTime = minLogoTime;
        else
            loadTime = Time.time;
    }

    private void Update()
    {
        if (Time.time < minLogoTime)
        {
            _fadeGroup.alpha = 1 - Time.time;
        }

        if (Time.time > minLogoTime && loadTime != 0)
        {
            _fadeGroup.alpha = Time.time - minLogoTime;
            if (_fadeGroup.alpha >= 1)
            {
                SceneManager.LoadScene("Lobby");
            }
        }
    }
}