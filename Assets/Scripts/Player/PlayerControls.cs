﻿using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using TMPro;
using UnityEngine;
using UnityEngine.UIElements;

public class PlayerControls : MonoBehaviour, IPunObservable
{
    private PhotonView _photonView;
    private SpriteRenderer _spriteRenderer;
    private Joystick _joystick;
    private Rigidbody2D _rigidbody;
    public Camera _camera;
    private float moveSpeed = 5f;
    public Transform cameraTarget;
    public float dampTime = 0.15f;
    private Vector3 velocity = Vector3.zero;
    [SerializeField] private Transform background;

    private Animator _animator;

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        throw new System.NotImplementedException();
    }

    void Start()
    {
        _camera = FindObjectOfType<Camera>();
        background = transform.GetChild(0);
        _rigidbody = GetComponent<Rigidbody2D>();
        _joystick = FindObjectOfType<Joystick>();
        _photonView = GetComponent<PhotonView>();
        _spriteRenderer = GetComponent<SpriteRenderer>();

        _animator = GetComponent<Animator>();
    }

    void Update()
    {
        if (!_photonView.IsMine)
        {
            _camera.enabled = false;
            background.gameObject.SetActive(false);
            return;
        }

        PlayerMovement();
    }

    void PlayerMovement()
    {
        _animator.SetFloat("PlayerSpeed", Mathf.Abs(_joystick.Horizontal));
        _rigidbody.velocity = new Vector2(_joystick.Horizontal * moveSpeed, _joystick.Vertical * moveSpeed);
        if (_joystick.Horizontal < 0)
            _spriteRenderer.flipX = true;
        else if (_joystick.Horizontal > 0)
            _spriteRenderer.flipX = false;
    }
}